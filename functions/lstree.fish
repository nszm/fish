function lstree --wraps='exa -T' --description 'alias lstree=exa -T'
  exa -T $argv
        
end
