function ls --wraps='exa -l --group-directories-first' --description 'alias ls=exa -l --group-directories-first'
  exa -l --group-directories-first $argv
        
end
