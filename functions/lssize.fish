function lssize --wraps='exa -l --sort=size' --description 'alias lssize=exa -l --sort=size'
  exa -l --sort=size $argv
        
end
