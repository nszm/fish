function lsdate --wraps='exa -l --sort=modified' --description 'alias lsdate=exa -l --sort=modified'
  exa -l --sort=modified $argv
        
end
