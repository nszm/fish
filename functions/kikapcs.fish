function kikapcs --wraps='systemctl poweroff' --description 'alias kikapcs=systemctl poweroff'
  systemctl poweroff $argv
        
end
